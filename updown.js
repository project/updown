
Drupal.behaviors.voteUpDown = function() {
  currentScore = $('.updown-widget').children('.updown-score').children('.updown-current-score');
  voteElement = $('.updown-widget').find('.updown-vote');
  voteUndoElement = $('.updown-widget').find('.updown-voteundo');

  // clicking on the '+' or '-' buttons
  voteElement.find('a').click(function() {
    $.get($(this).attr('href'), function(data) {
      data = Drupal.parseJson(data);
      $("#updown-widget-" + data.content_type +"-"+ data.content_id)
        .find('.updown-current-score')
        .html(data.score.toString())
        .end()
        .find('.updown-vote')
        .find('.updown-voteup a').attr('href', data.vote_full)
        .end()
        .find('.updown-votedown a').attr('href', data.vote_none)
        .end()
        .hide()
        .parents("#updown-widget-" + data.content_type +"-"+ data.content_id)
        .find(".updown-voteundo")
        .show()
        .find('a')
        .attr('href', data.vote_undo);
    });

    return false;
  });
  
  // clicking on the 'undo' button
  voteUndoElement.find('a').click(function() {
    $.get($(this).attr('href'), function(data) {
      data = Drupal.parseJson(data);
      $("#updown-widget-" + data.content_type +"-"+ data.content_id)
        .find('.updown-current-score')
        .html(data.score.toString())
        .end()
        .find('.updown-vote')
        .find('.updown-voteup a').attr('href', data.vote_full)
        .end()
        .find('.updown-votedown a').attr('href', data.vote_none)
        .end()
        .show()
        .parents("#updown-widget-" + data.content_type +"-"+ data.content_id)
        .find(".updown-voteundo")
        .hide()
        .find('a')
        .attr('href', data.vote_undo);
    });
    // disable the normal link
    return false;
  });
};
