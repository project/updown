<div class="updown-widget updown-widget-<?php print $vote_class; ?>" id="updown-widget-<?php print $content_type; ?>-<?php print $content_id; ?>">
  <div class="updown-score"><span class="updown-current-score"><?php print $current_score; ?></span> score</div>
  <div class="updown-vote">
    <div class="updown-voteup"><a href="<?php print $vote_up_uri; ?>"  <?php if (!$vote_down_uri) { print ' class="updown-nodown"'; }?>>+</a></div>
  <?php if ($vote_down_uri) : ?>
    <div class="updown-votedown"><a href="<?php print $vote_down_uri; ?>">–</a></div>
  <?php endif; ?>
  </div>
  <?php if ($vote_undo_uri) : ?>
    <div class="updown-voteundo"><a href="<?php print $vote_undo_uri; ?>">undo</a></div>
  <?php endif; ?>
</div>